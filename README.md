# Waterloo Hardware Chat Bot for Order Taking

<a href="https://whchatbot.herokuapp.com/"> Link to Application </a>


## Description
Waterloo Hardware's Chatbot is a an automated mobile application bot which allows users to place order. The app shows a link to the menu where all items are listed.
App also has an upsell feature wherein the Chatbot tries to upsell users with items on clearance sale.  When purchase is made final application shows the amount with
taxes and displays products bought.

### To run: 

#### Pre-requistes -

1. Load folder in Node.js editor like VS Code
2. Install node.js or "npm install"
3. Have Xampp server running


#### Steps to execute:

1. The first time run `npm install`
2. `SB_CLIENT_ID=<put_in_your_client_id> npm start
3. Open localhost url with port number
4. Enter "Hi" to start the application

Application will show up.

# OR

You can click the  below URL to see the live working demo of the project.

https://whchatbot.herokuapp.com/

There is a brief [presentation here](ES6Templates.pdf).

Made By - Vaneesh Jha
Template provided by - Richard Hildred