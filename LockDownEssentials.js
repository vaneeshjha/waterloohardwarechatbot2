const Order = require("./Order");

const OrderState = Object.freeze({
    WELCOMING:   Symbol("welcoming"),
    HomeH:   Symbol("homeh"),
    ITEM:   Symbol("item"),
    UPSELL: Symbol("upsell"),
});

module.exports = class LockDownEssentials extends Order{
    constructor(sNumber, sUrl){
        super(sNumber, sUrl);
        this.stateCur = OrderState.WELCOMING;
        this.sExtras = "";
        this.sInput="";
        this.sItem="";
        this.nTotal = 0;
    }
    handleInput(sInput){
        let aReturn = [];
        
        switch(this.stateCur){
            case OrderState.WELCOMING:
                this.stateCur = OrderState.ITEM;
                aReturn.push("Welcome to Waterloo Hardwares.");
                aReturn.push(`For a list of what we sell tap:`);
                aReturn.push(`${this.sUrl}/payment/${this.sNumber}/`);
                aReturn.push("To buy Enter the Product code");
                break;
            case OrderState.ITEM:
                if(sInput == 1){
                    this.sItem +="Dustbin";
                    this.nTotal += 7.99;
                    this.stateCur = OrderState.UPSELL;
                    
                  }
                else if(sInput == 2){
                    this.sItem +="Broom";
                    this.nTotal += 6.99
                    this.stateCur = OrderState.UPSELL;
                    
                    
                  }
                else if(sInput == 3){
                    this.sItem +="Snow Shovel";
                    this.nTotal += 8.99;
                    this.stateCur = OrderState.UPSELL;
                    
                    
                  }
                else if(sInput == 4){
                    this.sItem +="Cleaners";
                    this.nTotal += 3.99
                    this.stateCur = OrderState.UPSELL;
                  }

                  else if(sInput == 5){
                    this.sItem +="Furnance Filter";
                    this.nTotal += 12.99
                    this.stateCur = OrderState.UPSELL;
                  }
                  else {
                    aReturn.push("Please enter valid input.");
                    this.stateCur = OrderState.WELCOMING;
                    break;
                  }
                  aReturn.push("We have 1. Car Cloth , 2. Ear Buds , 3. Headlamps on Clearance!!!!\n Enter mentioned product code to buy. \n To Order more items from Main Menu press 4");
                  break;
            case OrderState.UPSELL:
              
                this.sExtras = sInput;
                if(sInput == 1){
                    this.sExtras="Car Cloth";
                    this.nTotal += 2.99;
                    
                    
                  }else if(sInput == 2){
                    this.sExtras="Ear buds";
                    this.nTotal += 2.99;

                    
                  }else if(sInput == 3){
                    this.sExtras="headlamps";
                    this.nTotal += 3.99;

                  }
                  else if(sInput == 4){
                    this.stateCur = OrderState.ITEM;
                    aReturn.push("To buy Enter the Product code");
                    break;

                  }
                  else {
                    this.nTotal += 0;
                    
                  }
                aReturn.push("Thank-you for your order of");
                aReturn.push(`${this.sItem} and ${this.sExtras} - Additional item`);
                this.nTotal = (this.nTotal * 1.13).toFixed(2);
                aReturn.push(`Your total comes to $${this.nTotal} after tax`);
                aReturn.push(`We will text you from 519-616-9999 when your order is ready or if we have questions.`)
                this.isDone(true);
                break;
        }
        return aReturn;
    }
    renderForm(){
      // your client id should be kept private
      return(`
      <html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="content-type">
			<style type="text/css">ol{margin:0;padding:0}table td,table th{padding:0}.c6{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:138.8pt;border-top-color:#000000;border-bottom-style:solid}.c1{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:156pt;border-top-color:#000000;border-bottom-style:solid}.c8{border-right-style:solid;padding:5pt 5pt 5pt 5pt;border-bottom-color:#000000;border-top-width:1pt;border-right-width:1pt;border-left-color:#000000;vertical-align:top;border-right-color:#000000;border-left-width:1pt;border-top-style:solid;border-left-style:solid;border-bottom-width:1pt;width:173.2pt;border-top-color:#000000;border-bottom-style:solid}.c10{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:24pt;font-family:"Arial";font-style:normal}.c7{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:17pt;font-family:"Arial";font-style:normal}.c3{color:#000000;font-weight:700;text-decoration:none;vertical-align:baseline;font-size:19pt;font-family:"Arial";font-style:normal}.c9{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:left}.c2{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:center}.c0{padding-top:0pt;padding-bottom:0pt;line-height:1.0;text-align:left}.c11{margin-left:auto;border-spacing:0;border-collapse:collapse;margin-right:auto}.c12{background-color:#ffffff;max-width:468pt;padding:72pt 72pt 72pt 72pt}.c5{height:11pt}.c4{height:0pt}.title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}</style>
		</head>
		<body class="c12">
			<p class="c2">
				<span class="c10">WELCOME TO WATERLOO HARDWARE</span>
			</p>
			<p class="c2 c5">
				<span class="c10"></span>
			</p>
			<p class="c2">
				<span class="c3">MENU (CURBSIDE PICKUP ONLY)</span>
			</p>
			<p class="c2 c5">
				<span class="c3"></span>
			</p>
			<p class="c2">
				<span class="c7">Text &ldquo;PICKUP&rdquo; to 519-616-9999 to place order</span>
			</p>
			<p class="c2 c5">
				<span class="c3"></span>
			</p>
			<p class="c2">
				<span class="c3">MAIN MENU </span>
			</p>
			<p class="c5 c9">
				<span class="c3"></span>
			</p>
			<a id="t.6558f93006687a2d39dda8f552d5a7495d8cf85c"></a>
			<a id="t.0"></a>
			<table class="c11">
				<tbody>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Product Code</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Product Name</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Price</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">1</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Dustbins</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">7.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">2</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Brooms</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">6.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">3</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Snow Shovel</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">8.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">4</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Cleaners</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">3.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c6" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">5</span>
							</p>
						</td>
						<td class="c8" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Furnace Filter</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">12.99</span>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<p class="c2 c5">
				<span class="c3"></span>
			</p>
			<p class="c9">
				<span class="c3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CLEARANCE</span>
			</p>
			<p class="c2 c5">
				<span class="c3"></span>
			</p>
			<a id="t.212ebcc045fdcab9c83013f20db64b6479b7608f"></a>
			<a id="t.1"></a>
			<table class="c11">
				<tbody>
					<tr class="c4">
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Product Code</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Product Name</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Price</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">1</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Car Cloth</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">2.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">2</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Ear Buds</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">2.99</span>
							</p>
						</td>
					</tr>
					<tr class="c4">
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">3</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">Headlamps</span>
							</p>
						</td>
						<td class="c1" colspan="1" rowspan="1">
							<p class="c0">
								<span class="c3">3.99</span>
							</p>
						</td>
					</tr>
				</tbody>
			</table>
			<p class="c2 c5">
				<span class="c3"></span>
			</p>
		</body>
	</html>s`);
  
    }
}
